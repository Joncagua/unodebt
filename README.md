# Unode
![OSHW](https://bitbucket.org/Joncagua/unodebt/raw/master/EC000001.png)

**[OSHW] EC000001 | Certified open source hardware |** [oshwa.org/cert.](https://www.oshwa.org/cert)

_La Pcb es un nodo bluetooth para aplicaciones IOT el cual tiene acelerometro, giroscopio, sensor de temperatura y sensor magnetico._

### Pre-requisitos 📋

_Altium 18 o superior_

### Instalación 🔧

_Solo se necesita cualquier version de altium superior al 18 para abrir el proyecto y editarlo._


### Y las pruebas de estilo de codificación ⌨️

_Para esta prueba puedes usar el codigo facilitado por Nordic el cual cuenta con algunos ejemplos, el siguiente link consta con ejemplo en el manejo de los sensores de la placa._

```
https://bitbucket.org/Joncagua/stm32-examples/src/master/
```

## Despliegue 📦

_Agrega notas adicionales sobre como hacer deploy_

## Construido con 🛠️

_Las herramientas usadas para hacer el nodo_

* [Altium](https://www.altium.com) - El software para hacer la PCB


## Wiki 📖

Ya vamos a desarrollar mas Fw para pruebas  con este equipo


## Autores ✒️

_Este proyecto tiene dos integrantes que hicieron tanto la parte de diseño con el ensamblaje de la misma_

* **Jonathan Cagua** - *Diseño de PCB Inicial* - [Joncagua](https://bitbucket.org/Joncagua)
* **Joffre Anzules** - *Ensamblaje y BOM* - [Jowlanzm86](https://bitbucket.org/Jowlanzm86)


## Licencia 📄

Hardware License: CERN Hardware released under an CERN Open Hardware Licence v1.2. See the LICENSE_HARDWARE file for more information.

## Expresiones de Gratitud 🎁

* Comenta a otros sobre este proyecto 📢
* Invita una cerveza 🍺 o un café ☕ a alguien del equipo. 
* Da las gracias públicamente 🤓.
* etc.



---
⌨️ con ❤️ por [joncagua](https://bitbucket.org/Joncagua) 😊
